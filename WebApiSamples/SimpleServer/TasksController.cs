﻿using System.Web.Http;

namespace SimpleServer
{
    [RoutePrefix("api/tasks")]
    public class TasksController : ApiController
    {
        [Route("")]
        public IHttpActionResult Get(string name)
        {
            return Ok("Hola " + name);
        }

        [Route("grabar")]
        public IHttpActionResult Post(Client client)
        {
            if (client.Type == "External")
                return BadRequest("Only Internal Clients");
            if (client.Type != "Internal") return BadRequest("Invalid Arguments");

            return Ok();

        }
    }
}
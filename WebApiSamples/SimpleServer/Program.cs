﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Topshelf;

namespace SimpleServer
{
    class Program
    {
        static void Main(string[] args)
        {
            // como consola
            //var server = new ApiContainer();
            //server.Start();
            //Console.WriteLine("Servidor esta levantado....");
            //Console.ReadKey();
            //server.Stop();

            //como servicio
            HostFactory.Run(host=>
            {
                host.Service<ApiContainer>(container =>
                {
                    container.ConstructUsing(()=> new ApiContainer());
                    container.WhenStarted((svc, hostControl) => svc.Start(hostControl));
                    container.WhenStopped((svc, hostControl) => svc.Stop(hostControl));
                    container.WhenShutdown((svc, hostControl) => svc.Stop(hostControl));

                });

                host.SetDescription("Este es un ejemplo de un SelfHosted WebAPI como sevicio usando TopShelf");
                host.SetDisplayName("Simple Api");
                host.SetServiceName("SimpleApi");
            });
        }
    }
}

﻿using System;
using System.Web.Http;
using System.Web.Http.SelfHost;
using Topshelf;

namespace SimpleServer
{
    public class ApiContainer : ServiceControl
    {
        private HttpSelfHostServer server;
        public ApiContainer()
        {
            var baseAddress = "http://localhost:8092";
            var config = new HttpSelfHostConfiguration(baseAddress);
            new Bootstrap().Configure(config);
            server = new HttpSelfHostServer(config);
        }

        public bool  Start(HostControl control )
        {
            try
            {
                server.OpenAsync().Wait();
                return true;
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.InnerException.Message);
                //netsh http add urlacl url=http://+:8092/ user=machine\username
                return false;
            }
        }

        public bool Stop(HostControl control)
        {
            server.CloseAsync();
            server.Dispose();
            return true;
        }
    }
}
﻿"use strict";
budgetMe.controller('BudgetListController',
    function ($scope, budgetMeData, $window ) {

        $scope.budgets = budgetMeData.getAllBudgets();
    }
);
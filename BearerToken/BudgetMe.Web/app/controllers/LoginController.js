﻿"use strict";
budgetMe.controller('LoginController', function ($scope, $window, Authentication) {
    $scope.userLogon = {
        userName: "",
        password: "",
        rememberMe: false
    }

    $scope.title = "Wellcome";
    $scope.message = "";

    $scope.login=function(loginModel) {
        Authentication.logIn(loginModel).then(function(response) {
            $scope.currentUser = loginModel.userName;
            $window.location.href = '/';
        },function (err) {
            $scope.message = err.error_description;

        });
    }
    $scope.forgetPassword = function () {
        //redirect to forget password page
    }

});
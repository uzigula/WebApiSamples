﻿"use strict";
budgetMe.controller('BudgetController',
    function ($scope, $log, budgetMeData, $routeParams, BudgetFormatters) {

        budgetMeData.getBudget($routeParams.id).then(
            function(budget) {
                $log.info(budget);
                $scope.budget = budget;
            },
            function (response) {
                $log.warn('Error');
                 $log.warn(response);
            }
        );

        $scope.myClase = function(value) {
            if (value>=0)
                return "badge-green";
            else {
                return "badge-red";
            }
        }
        $scope.valueFormatted = function(budgetItem) {
            return BudgetFormatters.currency(budgetItem.amount, budgetItem.currency);
        }

        $scope.outcomeValueFormatted = function(outcome) {
            if (outcome.isNominal === true)
                return BudgetFormatters.currency(outcome.value, outcome.currency);
            else
               return BudgetFormatters.percentage(outcome.value);

        }
    }
);
﻿"use strict";
budgetMe.factory("budgetMeData", function ($resource, $q) {
    var resource = $resource('http://localhost:37477/api/Budget/:id', { id: '@id' });
    return {
        getBudget: function (id) {
            var defered = $q.defer();
            resource.get({ id: id },
                function (budget) {
                    defered.resolve(budget);
                },
                function (response) {
                    defered.reject(response);
                }
            );
            return defered.promise;

        },
        getAllBudgets: function () {
            return resource.query();
        }
    };
});
﻿"use strict";
budgetMe.factory("BudgetFormatters", function ($filter)
{
    return {
        currency : function (value,currency){
            return currency + " " + $filter("number")(value, 2);
        },

        percentage: function(value) {
            return $filter("number")(value * 100, 2) + " %";
        }
    }

});
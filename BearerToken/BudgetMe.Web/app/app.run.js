'use strict';
budgetMe.run(function ($rootScope, $window, authStorage) {

    $rootScope.getInitialUrl = function () {
        return $rootScope.initialUrl;
    };
    $rootScope.setInitialUrl = function (url) {
        $rootScope.initialUrl = url;
    };

    $rootScope.currentUser = null;

    $rootScope.setInitialUrl("/landing");

    $rootScope.$on("$routeChangeStart", function (event, next, current) {
        if (!authStorage.isAuthenticated()) {
            event.preventDefault();
            $window.location.href = "login.html";
        } else {
            var authData = authStorage.getToken();
            $rootScope.currentUser = authData.userName;
        }
    });
});


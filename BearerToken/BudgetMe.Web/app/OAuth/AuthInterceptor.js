﻿'use strict';
budgetMe.factory('AuthInterceptor', function ($q, $window, authStorage) {

    var authInterceptorServiceFactory = {};

    var _request = function (config) {

        config.headers = config.headers || {};

        var authData = authStorage.getToken();
        if (authData) {
            config.headers.Authorization = 'Bearer ' + authData.token;
        }

        return config;
    }

    var _responseError = function (rejection) {
        if (rejection.status === 401) {
            if (!authStorage.isAuthenticated())
                $window.location.href = 'login.html';
            else {
                // mostrar pagina de error
            }
        }
        return $q.reject(rejection);
    }

    authInterceptorServiceFactory.request = _request;
    authInterceptorServiceFactory.responseError = _responseError;

    return authInterceptorServiceFactory;
});
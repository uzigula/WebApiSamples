﻿"use strict";
budgetMe.factory('Authentication', function ($log, $http, $q, authStorage) {
    
    var urlbase = "http://localhost:37477";
    var authService = {};

    var _logIn = function (loginData) {
        // post
        $log.info("autenticando:");
        $log.info(loginData);

        var data = "grant_type=password&username=" + loginData.userName + "&password=" + loginData.password;

        var deferred = $q.defer();

        $http.post(urlbase+'/token', data, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } })
            .success(function(response) {

                authStorage.saveToken({ token: response.access_token, userName: loginData.userName });
                deferred.resolve(response);

            }).error(function(err, status) {
                _logOut();
                deferred.reject(err);
            });
        return deferred.promise;
    };

    var _logOut = function () {
        authStorage.removeToken();
    };


    authService.logIn = _logIn;
    authService.logOut = _logOut;

    return authService;
});
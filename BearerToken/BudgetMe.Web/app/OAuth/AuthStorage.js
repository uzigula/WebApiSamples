﻿'use strict';
budgetMe.factory('authStorage', function (localStorageService) {
    var tokenkey = "authorizationData";
    var authStorageFactory = {};

    authStorageFactory.isAuthenticated = function () {
        var token = _getToken();
        return (token != null);
    };


    var _getToken = function () {
        return localStorageService.get(tokenkey);
    }

    var _saveToken = function(token) {
        localStorageService.set(tokenkey, token);
    }

    var _removeToken = function() {
        localStorageService.remove(tokenkey);
    }
    authStorageFactory.getToken = _getToken;
    authStorageFactory.saveToken = _saveToken;
    authStorageFactory.removeToken = _removeToken;


    return authStorageFactory;
});
﻿'use strict';

var budgetMe = angular.module('budgetMe', ['ngResource', 'ngRoute','LocalStorageModule']);

budgetMe.config(function ($routeProvider,$httpProvider) {
    $routeProvider.when('/landing',
    {
        templateUrl: 'Login.html',
        controller: 'BudgetListController'
    });

    $routeProvider.when('/login',
    {
        templateUrl: 'Login.html',
        controller: 'LoginController'
    });

    $routeProvider.when('/budgets',
    {
        templateUrl: 'Views/Budget/List.html',
        controller: 'BudgetListController'
    });
    $routeProvider.when('/budget/:id',
    {
        templateUrl: 'Views/Budget/budget.html',
        controller: 'BudgetController'

    });

    $routeProvider.otherwise({
        redirectTo: '/budgets'
    });

    $httpProvider.interceptors.push('AuthInterceptor');
});




budgetMe.filter('percentage', [
    '$filter', function ($filter) {
        return function (input, decimals) {
            return $filter('number')(input * 100, decimals) + '%';
        };
    }
]);


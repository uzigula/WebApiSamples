﻿using System.Collections.Generic;
using System.Linq;

namespace BudgetMe.Api.Models
{
    public class Budget
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<BudgetItem> Expenses { get; set; }
        public IEnumerable<BudgetItem> Incomes { get; set; }

        public IEnumerable<OutcomeItem> Outcomes
        {
            get
            {
                return getOutComes();
            }
        }

        public decimal TotalExpenses
        {
            get { return this.Expenses.Sum(x => x.Total()); }
        }

        public decimal TotalIncomes
        {
            get { return this.Incomes.Sum(x => x.Total()); }
        }


        private IEnumerable<OutcomeItem> getOutComes()
        {
            var resultado = new OutcomeItem() { Name = "Profit", IsNominal = true, Currency = "PEN"};
            resultado.Value = (double)(this.Incomes.Sum(x => x.Total()) - this.Expenses.Sum(x => x.Total()));

            var van = new OutcomeItem() { Name = "NPV", Value = 4500, IsNominal = true, Currency = "PEN"};
            var irr = new OutcomeItem() { Name = "IRR", Value = 0.125, IsNominal = false};
            return new List<OutcomeItem>() { resultado, van, irr };
        }
    }
}
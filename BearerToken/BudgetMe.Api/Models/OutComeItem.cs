﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BudgetMe.Api.Models
{
    public class OutcomeItem
    {
        public string Name { get; set; }
        public double Value { get; set; }
        public string Currency { get; set; }
        public bool IsNominal { get; set; }
    }
}

﻿using System;

namespace BudgetMe.Api.Models
{
    public class DetailBudgetItem
    {
        public string Description { get; set; }
        public DateTime DueDate { get; set; }
        public decimal Amount { get; set; }
        public string Currency  {get; set; }
        public bool Paid { get; set; }
    }
}
﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace BudgetMe.Api.Models
{
    public class BudgetItem
    {
        public string Name { get; set; }
        public IEnumerable<DetailBudgetItem> Items { get; set; }


        public decimal Total()
        {
           return  Items.Sum(x => x.Currency == "USD" ? x.Amount*3 : x.Amount);
        }
    }
}
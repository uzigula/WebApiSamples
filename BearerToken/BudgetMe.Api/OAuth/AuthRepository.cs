﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace BudgetMe.Api.OAuth
{
    public class AuthRepository :IDisposable
    {
        private AuthContext _context; // contexto de EF

        private UserManager<ApplicationUser> _userManager;

        public AuthRepository()
        {
            _context =  new AuthContext();
            _userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(_context));
        }

        public async Task<ApplicationUser> FindUser(string userName, string password)
        {
            var user = await _userManager.FindAsync(userName, password);
            return user;
        }

        public void Dispose()
        {
            _context.Dispose();
            _userManager.Dispose();
        }

        public async Task<IdentityResult> RegisterUser(Models.UserViewModel userModel)
        {
            ApplicationUser user = new ApplicationUser()
            {
                UserName = userModel.UserName
            };
            //var user = new IdentityUser(userModel.UserName);
            var result = await _userManager.CreateAsync(user, userModel.Password);
            return result;
        }
    }
}

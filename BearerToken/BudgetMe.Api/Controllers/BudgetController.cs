﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using BudgetMe.Api.Models;

namespace BudgetMe.Api.Controllers
{
    [Authorize]
    [RoutePrefix("api/Budget")]
    public class BudgetController : ApiController
    {
        private IList<Budget> list;
        public BudgetController()
        {
            list = new List<Budget>();
            list.Add(GetBudget("Default", 1));
            list.Add(GetBudget("Ideal", 2));
            list.Add(GetBudget("Another Budget", 3));

        }
        [Route("")]
        public IHttpActionResult Get()
        {
            return Ok(list);
        }

        [Route("{id:int}")]
        public IHttpActionResult Get(int id)
        {
            var budget = list.FirstOrDefault(x => x.Id == id);
            if (budget == null) return NotFound();
            return Ok(budget);
        }

        private Budget GetBudget(string name, int id)
        {
            var gastosVencidos = new List<DetailBudgetItem>();
            gastosVencidos.Add(new DetailBudgetItem()
            {
                Description = "Deuda IBK",
                Amount = 1000*id,
                Currency = "PEN",
                DueDate = new DateTime(2015, 04, 01),
                Paid = false
            });
            gastosVencidos.Add(new DetailBudgetItem()
            {
                Description = "Deuda BCP",
                Amount = 1000*id,
                Currency = "PEN",
                DueDate = new DateTime(2015, 04, 01),
                Paid = false
            });
            gastosVencidos.Add(new DetailBudgetItem()
            {
                Description = "Decameron",
                Amount = 70*id,
                Currency = "USD",
                DueDate = new DateTime(2015, 04, 01),
                Paid = false
            });


            var gastosVigentes = new List<DetailBudgetItem>();
            gastosVigentes.Add(new DetailBudgetItem()
            {
                Description = "Movilidad",
                Amount = 200 * id,
                Currency = "PEN",
                DueDate = new DateTime(2015, 04, 01),
                Paid = false
            });
            gastosVigentes.Add(new DetailBudgetItem()
            {
                Description = "Pacifico Vida",
                Amount = 65 * id,
                Currency = "USD",
                DueDate = new DateTime(2015, 04, 01),
                Paid = false
            });
            gastosVigentes.Add(new DetailBudgetItem()
            {
                Description = "Mantenimiento",
                Amount = 300 * id,
                Currency = "PEN",
                DueDate = new DateTime(2015, 04, 01),
                Paid = false
            });


            var ingresosVencidos = new List<DetailBudgetItem>();
            ingresosVencidos.Add(new DetailBudgetItem()
            {
                Description = "Sueldo Belax",
                Amount = 2500 * id,
                Currency = "PEN",
                DueDate = new DateTime(2015, 04, 01),
                Paid = false
            });
            ingresosVencidos.Add(new DetailBudgetItem()
            {
                Description = "Headlight",
                Amount = 1000 * id,
                Currency = "PEN",
                DueDate = new DateTime(2015, 04, 01),
                Paid = false
            });
            ingresosVencidos.Add(new DetailBudgetItem()
            {
                Description = "ISIL",
                Amount = 300 * id,
                Currency = "USD",
                DueDate = new DateTime(2015, 04, 01),
                Paid = false
            });


            var ingresosVigentes = new List<DetailBudgetItem>();
            ingresosVigentes.Add(new DetailBudgetItem()
            {
                Description = "Freelo",
                Amount = 200 * id,
                Currency = "PEN",
                DueDate = new DateTime(2015, 04, 01),
                Paid = false
            });
            ingresosVigentes.Add(new DetailBudgetItem()
            {
                Description = "otro Freelo",
                Amount = 65 * id,
                Currency = "USD",
                DueDate = new DateTime(2015, 04, 01),
                Paid = false
            });

            var budget = new Budget() {Name = name,Id = id};

            budget.Expenses = new List<BudgetItem>()
            {
                new BudgetItem() {Name = "Default", Items = gastosVencidos},
                new BudgetItem() {Name = "Active", Items = gastosVigentes}
            };
            budget.Incomes = new List<BudgetItem>()
            {
                new BudgetItem() {Name = "Default", Items = ingresosVencidos},
                new BudgetItem() {Name = "Active", Items = ingresosVigentes}
            };
            
            return budget;
        }
    }

    
}